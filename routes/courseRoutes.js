const express = require("express");
const courseController = require("../controllers/courseControllers");
const router = express.Router();
const auth = require("../auth");

router.get("/", (req, res) => {
  courseController.getAllCourses().then((result) => {
    res.send(result);
  });
});

router.post("/create-course", auth.verify, (req, res) => {
  courseController.createCourse(req.body).then((result) => {
    res.send(result);
  });
});

router.get("/active-courses", auth.verify, (req, res) => {
  courseController.getActiveCourses().then((result) => {
    res.send(result);
  });
});

router.get("/get-course", (req, res) => {
  courseController.getSpecificCourse(req.body.courseName).then((result) => {
    res.send(result);
  });
});

router.get("/:id", (req, res) => {
  courseController.getById(req.params.id).then((result) => {
    res.send(result);
  });
});

router.put("/archive-course", auth.verify, (req, res) => {
  courseController.archieveCourse(req.body.courseName).then((result) => {
    res.send(result);
  });
});

router.put("/unarchive-course", auth.verify, (req, res) => {
  courseController.unarchieveCourse(req.body.courseName).then((result) => {
    res.send(result);
  });
});

router.put("/archive/:id", auth.verify, (req, res) => {
  courseController.archieveCourseById(req.params.id).then((result) => {
    res.send(result);
  });
});

router.put("/unarchive/:id", auth.verify, (req, res) => {
  courseController.unArchieveCourseById(req.params.id).then((result) => {
    res.send(result);
  });
});

router.delete("/delete-course", auth.verify, (req, res) => {
  courseController.deleteCourse(req.body.courseName).then((result) => {
    res.send(result);
  });
});

router.delete("/delete/:id", auth.verify, (req, res) => {
  courseController.deleteCourseById(req.params.id).then((result) => {
    res.send(result);
  });
});

// update course
router.put("/:courseId/edit", auth.verify, (req, res) => {
  courseController.editCourse(req.params.courseId, req.body).then((result) => {
    res.send(result);
  });
});

module.exports = router;
