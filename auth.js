const jwt = require("jsonwebtoken");
const secret = "CourseBookingAPI";

module.exports.createAccessToken = (user) => {
  const { _id, email, isAdmin } = user;
  const data = {
    id: _id,
    email: email,
    isAdmin: isAdmin,
  };

  return jwt.sign(data, secret, {});
};

// .sign(<data>,<secret_key>, ()=>{})

module.exports.verify = (req, res, next) => {
  // get the token in the headers authorization
  let token = req.headers.authorization;
  token = token.slice(7, token.length);
  if (token) {
    return jwt.verify(token, secret, (err, data) => {
      if (err) {
        return res.send({ auth: "failed" });
      } else {
        next();
      }
    });
  }
};

module.exports.decode = (token) => {
  if (token) {
    let decoded = jwt.decode(token.slice(7, token.length), { complete: true });
    return decoded.payload;
  }
};
